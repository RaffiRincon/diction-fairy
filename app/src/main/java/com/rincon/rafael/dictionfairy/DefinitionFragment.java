package com.rincon.rafael.dictionfairy;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DefinitionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DefinitionFragment extends Fragment {

    final static String WORDS_KEY = "words";
    final static String MAX_WIDTH_KEY = "maxWidth";

    public DefinitionFragment() {
        // Required empty public constructor
    }

    public static DefinitionFragment newInstance() {
        return new DefinitionFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_definition, container, false);

        // get words from arguments if they exist there
        if (getArguments() != null) {
            String[] words = getArguments().getStringArray(WORDS_KEY);
            int maxWidth = getArguments().getInt(MAX_WIDTH_KEY, -1);
            if (maxWidth != -1 && words != null) {
                populateWords(words, view, maxWidth);
            }
        }
        return view;
    }

    private void populateWords(final String[] words, View rootView, int maxWidth) {
        if (words == null || words.length == 0) {
            return;
        }

        LinearLayout row = new LinearLayout(getContext());
        row.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout definitionLineContainer = rootView.findViewById(R.id.definition_line_container);
        definitionLineContainer.addView(row);
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());

        for (int i = 0; i < words.length; i++) {
            View wordInDefinitionView = layoutInflater.inflate(R.layout.definition_item_view, row, false);
            TextView wordInDefinitionTextView = wordInDefinitionView.findViewById(R.id.word_text_view);

            wordInDefinitionTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Activity parentActivity = getActivity();
                    if (!(parentActivity instanceof DefinitionActivity)) {
                        return;
                    }
                    DefinitionActivity parentDefinitionActivity = (DefinitionActivity) parentActivity;

                    parentDefinitionActivity.getAndPopulateWords(((TextView)v).getText().toString());
                }
            });
            wordInDefinitionTextView.setText(words[i]);
            row.addView(wordInDefinitionView);

            wordInDefinitionView.measure(0, 0);
            row.measure(0, 0);

            if (row.getMeasuredWidth() > maxWidth) {
                row.removeView(wordInDefinitionView);
                i--;
                row = new LinearLayout(getContext());
                row.setOrientation(LinearLayout.HORIZONTAL);
                definitionLineContainer.addView(row);
            }
        }
    }
}

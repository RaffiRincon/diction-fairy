package com.rincon.rafael.dictionfairy;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.NavUtils;
import android.support.v4.util.Pair;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Request;
import okhttp3.Response;

public class DefinitionActivity extends AppCompatActivity {

    static final String QUERY_KEY = "query";
    static final String DEFINITION_ENTRIES_KEY = "definitions";
    static final String CURRENT_DEFINITION_ENTRY_KEY = "currentDefinition";

    /**
     * An ArrayList that functions as a Stack that holds the previous definition entries for
     * use during backward navigation. Type is ArrayList not List because we store this in
     * onSaveInstanceState.
     */
    ArrayList<DefinitionEntry> previousDefinitionEntries = new ArrayList<>();
    DefinitionEntry currentDefinitionEntry;

    @BindView(R.id.definition_activity_ad_view) AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_definition);
        ButterKnife.bind(this);

        MobileAds.initialize(this, BuildConfig.AdMobID);
        loadAd();

        if (BuildConfig.DUMMY_DATA) {
            handleNewDefinition(DummyDataUtil.getDummyDefinitions()[0]);
            return;
        }

        if (savedInstanceState != null) {
            // load entries from savedInstanceState
            ArrayList<DefinitionEntry> definitionEntriesFromSavedInstanceState =
                    savedInstanceState.getParcelableArrayList(DEFINITION_ENTRIES_KEY);
            DefinitionEntry savedCurrentDefinition = savedInstanceState.getParcelable(CURRENT_DEFINITION_ENTRY_KEY);

            if (definitionEntriesFromSavedInstanceState != null && definitionEntriesFromSavedInstanceState.size() > 0) {
                previousDefinitionEntries = definitionEntriesFromSavedInstanceState;
                currentDefinitionEntry = savedCurrentDefinition;
                populateUIWithCurrentDefinition();
                return;
            }
        }

        // started from SearchActivity
        Intent startThis = getIntent();
        if (startThis != null && startThis.hasExtra(QUERY_KEY)) {
            String wordToDefine = startThis.getStringExtra(QUERY_KEY);
            getAndPopulateWords(wordToDefine);
            ActionBar supportActionBar = getSupportActionBar();
            if (supportActionBar != null)
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(CURRENT_DEFINITION_ENTRY_KEY, currentDefinitionEntry);
        outState.putParcelableArrayList(DEFINITION_ENTRIES_KEY, previousDefinitionEntries);
        super.onSaveInstanceState(outState);
    }

    void getAndPopulateWords(String wordToDefine) {
        if (NetworkUtil.deviceIsConnectedToInternet(this)) {
            new PopulateWordsTask().execute(new Pair<>(wordToDefine, this));
        } else {
            handleNoNetworkError();
        }
    }

    static class PopulateWordsTask extends AsyncTask<Pair<String, DefinitionActivity>, Void, DefinitionEntry> {
        WeakReference<DefinitionActivity> definitionActivityWeakReference;

        @Override
        protected DefinitionEntry doInBackground(Pair<String, DefinitionActivity>... pairs) {
            if (pairs == null || pairs.length == 0 || pairs[0] == null)
                return null;

            definitionActivityWeakReference = new WeakReference<>(pairs[0].second);
            DefinitionActivity activity = definitionActivityWeakReference.get();
            if (activity == null) {
                return null;
            }

            String query = pairs[0].first;
            Request request = NetworkUtil.requestForRawQuery(query);

            Response response = NetworkUtil.getResponseFromRequest(request);
            DefinitionEntry resultingEntry = DefinitionUtil.definitionEntryFromResponse(response);

            return resultingEntry;
        }

        @Override
        protected void onPostExecute(DefinitionEntry definitionEntry) {
            DefinitionActivity definitionActivity = definitionActivityWeakReference.get();
            if (definitionActivity != null) {
                definitionActivity.handleDefinitionFromNetwork(definitionEntry);
            }
        }
    }

    private void handleDefinitionFromNetwork(DefinitionEntry definitionEntry) {
        if (definitionEntry != null) {
            handleNewDefinition(definitionEntry);
        } else {
            handleNoDefinitionFoundError();
            if (currentDefinitionEntry == null) {
                navigateUp();
            }
        }
    }

    private void handleNoNetworkError() {
        Toast.makeText(this, getString(R.string.please_check_your_connection), Toast.LENGTH_SHORT).show();
    }

    private void handleNoDefinitionFoundError() {
        Toast.makeText(this, getString(R.string.no_definition_error_message), Toast.LENGTH_SHORT).show();
    }


    private void handleNewDefinition(@NonNull DefinitionEntry definitionEntry) {
        if (!DefinitionUtil.definitionCanBeUsedToPopulateUI(definitionEntry)) {
            handleNoDefinitionFoundError();
            goToPreviousWord();
            return;
        }

        if (currentDefinitionEntry != null)
            previousDefinitionEntries.add(currentDefinitionEntry);

        currentDefinitionEntry = definitionEntry;
        populateUIWithCurrentDefinition();
    }

    private void populateUIWithCurrentDefinition() {
        setTitle(currentDefinitionEntry.wordDefined);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int maxWidth = displayMetrics.widthPixels - (int)getResources().getDimension(R.dimen.definition_text_view_padding);
        String[] wordsInDefinition = DefinitionUtil.wordsIncludingPunctuationRemovingBraces(currentDefinitionEntry.definition);

        DefinitionFragment definitionFragment = new DefinitionFragment();
        Bundle definitionFragmentArguments = new Bundle();
        definitionFragmentArguments.putInt(DefinitionFragment.MAX_WIDTH_KEY, maxWidth);
        definitionFragmentArguments.putStringArray(DefinitionFragment.WORDS_KEY, wordsInDefinition);
        definitionFragment.setArguments(definitionFragmentArguments);
        getSupportFragmentManager().beginTransaction().replace(R.id.definition_fragment_container, definitionFragment).commit();
    }

    private void goToPreviousWord() {
        currentDefinitionEntry = previousDefinitionEntries.remove(previousDefinitionEntries.size() - 1);
        populateUIWithCurrentDefinition();
    }

    private void navigateUp() {
        NavUtils.navigateUpFromSameTask(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                if (previousDefinitionEntries.size() > 0)
                    goToPreviousWord();
                else
                    navigateUp();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }
}

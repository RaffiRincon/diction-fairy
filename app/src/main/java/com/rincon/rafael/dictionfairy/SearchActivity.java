package com.rincon.rafael.dictionfairy;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class SearchActivity extends AppCompatActivity {

    @BindView(R.id.search_activity_ad_view) AdView adView;
    @BindView(R.id.search_edit_text) EditText searchEditText;

    private final TextView.OnEditorActionListener editorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
            Timber.d("onEditorAction");
            if (event == null) {
                return false;
            }

            // user tapped enter
            if ((event.getAction() == KeyEvent.ACTION_DOWN)
                    && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                Context context = textView.getContext();

                if (BuildConfig.DUMMY_DATA) {
                    Intent startDefinitionActivityWithNoWords = new Intent(context, DefinitionActivity.class);
                    context.startActivity(startDefinitionActivityWithNoWords);
                    return true;
                }

                String searchQuery = searchEditText.getText().toString();
                String sanitizedQuery = DefinitionUtil.stringSanitizedForSearch(searchQuery);
                if (sanitizedQuery.length() == 0) {
                    handleIncorrectInputError();
                    return false;
                }

                Intent startDefinitionActivity = new Intent(context, DefinitionActivity.class);
                startDefinitionActivity.putExtra(DefinitionActivity.QUERY_KEY, sanitizedQuery);
                context.startActivity(startDefinitionActivity);
                return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        MobileAds.initialize(this, BuildConfig.AdMobID);
        loadAd();
        configureSearchEditText();
    }

    private void configureSearchEditText() {
        searchEditText.setOnEditorActionListener(this.editorActionListener);
    }

    private void handleIncorrectInputError() {
        Toast.makeText(this, getString(R.string.incorrect_input_error_message), Toast.LENGTH_SHORT).show();
    }

    private void loadAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }
}

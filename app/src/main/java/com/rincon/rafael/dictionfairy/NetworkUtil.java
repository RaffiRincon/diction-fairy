package com.rincon.rafael.dictionfairy;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.annotation.Nullable;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class NetworkUtil {

    private static final String SCHEME = "https";
    private static final String PATH = "api/v1/entries/en/";
    private static final String AUTHORITY = "od-api.oxforddictionaries.com";
    private static final String REGION_KEY = "regions";
    private static final String REGION_VALUE = "us";

    private static final String API_KEY_KEY = "app_key";
    private static final String API_KEY = BuildConfig.OxfordAPIKey;

    private static final String APP_ID_KEY = "app_id";
    private static final String APP_ID = BuildConfig.OxfordAppID;


    static Response getResponseFromRequest(Request request) {
        OkHttpClient client = new OkHttpClient();

        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    static Request requestForRawQuery(String rawQuery) {
        String sanitizedQuery = DefinitionUtil.stringSanitizedForSearch(rawQuery);
        Uri uri = new Uri.Builder()
                .scheme(SCHEME)
                .path(PATH)
                .appendPath(sanitizedQuery)
                .authority(AUTHORITY)
                .appendQueryParameter(REGION_KEY, REGION_VALUE)
                .build();

        Request request = new Request.Builder()
                .url(uri.toString())
                .get()
                .addHeader(APP_ID_KEY, APP_ID)
                .addHeader(API_KEY_KEY, API_KEY)
                .build();

        return request;
    }

    static boolean deviceIsConnectedToInternet(Context context) {
        ConnectivityManager cm =
            (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected;
        if (activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting()) isConnected = true;
        else isConnected = false;

        return isConnected;
    }
}

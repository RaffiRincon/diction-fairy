package com.rincon.rafael.dictionfairy;

import android.os.Parcel;
import android.os.Parcelable;

public class DefinitionEntry implements Parcelable {
    String wordDefined;
    String definition;

    DefinitionEntry() {

    }

    protected DefinitionEntry(Parcel in) {
        wordDefined = in.readString();
        definition = in.readString();
    }

    public static final Creator<DefinitionEntry> CREATOR = new Creator<DefinitionEntry>() {
        @Override
        public DefinitionEntry createFromParcel(Parcel in) {
            return new DefinitionEntry(in);
        }

        @Override
        public DefinitionEntry[] newArray(int size) {
            return new DefinitionEntry[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(wordDefined);
        dest.writeString(definition);
    }
}

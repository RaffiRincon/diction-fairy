package com.rincon.rafael.dictionfairy;

import android.os.Build;
import android.support.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Response;
import timber.log.Timber;

public class DefinitionUtil {

    static String stringSanitizedForSearch(String raw) {
        return raw.replaceAll("\\W+", "");
    }

    static String[] wordsIncludingPunctuationRemovingBraces(String original) {
        String regexLeftBracToLetterWithPipeAfter = "[{][^{}|]*([|]?=|)";
        String regexPipeToPipeWithLettersInBetween = "[|]\\w*[|]";
        String regexRightBrace = "[}]";
        String regexOr = "|";

        String regexToRemove =  regexLeftBracToLetterWithPipeAfter
                + regexOr
                + regexPipeToPipeWithLettersInBetween
                + regexOr
                + regexRightBrace;
        String regexThatMatchesContiguousWhitespaceCharacters = "[\\s*]";
        String[] words = original.replaceAll(regexToRemove, "")
                .split(regexThatMatchesContiguousWhitespaceCharacters);

        return words;
    }


    @Nullable
    static DefinitionEntry definitionEntryFromResponse(Response response) {
        DefinitionEntry definitionEntry = null;

        try {
            JSONObject responseJSON = new JSONObject(response.body().string());
            definitionEntry = new DefinitionEntry();

            JSONObject firstResult = responseJSON.getJSONArray("results").getJSONObject(0);
            String wordDefined = firstResult.getString("word");
            String definition = firstResult
                    .getJSONArray("lexicalEntries")
                    .getJSONObject(0)
                    .getJSONArray("entries")
                    .getJSONObject(0)
                    .getJSONArray("senses")
                    .getJSONObject(0)
                    .getJSONArray("definitions")
                    .getString(0);

            definitionEntry.wordDefined = wordDefined;
            definitionEntry.definition = definition;

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return  definitionEntry;
    }

    static boolean definitionCanBeUsedToPopulateUI(@Nullable DefinitionEntry definitionEntry) {
        if (definitionEntry == null)
            return false;

        if (definitionEntry.wordDefined == null || definitionEntry.wordDefined.length() == 0)
            return false;

        if (definitionEntry.definition == null || definitionEntry.definition.length() == 0)
            return false;

        return true;
    }
}

package com.rincon.rafael.dictionfairy;

public class DummyDataUtil {

    static DefinitionEntry[] getDummyDefinitions() {
        DefinitionEntry alphabet = new DefinitionEntry();
        alphabet.wordDefined = "Numbers";
        StringBuilder numbersBuilder = new StringBuilder();

        for (int i = 0; i < 40; i++) {
            for (int j = 0; j < 5; j++) {
                numbersBuilder.append(i);
            }
            numbersBuilder.append(" ");
        }

        alphabet.definition = numbersBuilder.toString();

        DefinitionEntry lexiconDefinition = new DefinitionEntry();
        lexiconDefinition.wordDefined = "Lexicon";
        lexiconDefinition.definition = "the vocabulary of a person, language, or branch of knowledge.";

        DefinitionEntry knowledgeDefinition = new DefinitionEntry();
        knowledgeDefinition.wordDefined = "Knowledge";
        knowledgeDefinition.definition = "facts, information, and skills acquired by a person through experience or education; the theoretical or practical understanding of a subject.";

        DefinitionEntry informationDefinition = new DefinitionEntry();
        informationDefinition.wordDefined = "information";
        informationDefinition.definition = "facts provided or learned about something or someone.";

        return new DefinitionEntry[]{alphabet, lexiconDefinition, knowledgeDefinition, informationDefinition};
    }

}
